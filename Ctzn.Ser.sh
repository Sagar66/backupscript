#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/cpanel/composer/bin:/root/bin:/opt/cpanel/composer/bin
set -o errexit
set -o nounset
################## Local VAR Group (SET VAR'S CAREFULLY) ###################
domainsPath=/home/expressbank/expressbank/glassfish/domains/    #Plz include / at last
domainName="domain1"                                          #domain name clean | don't include /
rotate=1                               #After the defined day's files will be processed(days)
logFile=Master.log                                              #Master log file name
mode="2"                                #Backup->Backup or Delete->delete files etc..
################ +Globals Don't Change ##################################
today=$('date' '+%Y-%m-%d@%H:%M:%S') # (: gives issue in compression use: --force-local)
fullPath=$domainsPath$domainName/logs
oldLogs=$fullPath/oldLogs
foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate)
backupFile=${fullPath}/${logFile}
################## Functions ################################
files(){
        if [ "$foundFiles" = "" ]; then
                echo "##################### ${today} ###################" >> ${backupFile}
                echo "No older then $rotate days files found" >> $backupFile
                echo "#############################################################" >> $backupFile
                exit;
        fi
}
fileCheck(){
        if [ ! -e $backupFile ]; then
                touch $backupFile
        fi
}
domainCheck(){
        if [ ! -d $fullPath ]; then
                echo "#######################################";
                echo "Domain dosen't seems to be exists";
                echo "#######################################";
                exit;
        fi
}
domainCheck
fileCheck
mkdir -p $fullPath/oldLogs

directDelete(){
        files
        echo "############# Delete@$today ###############" >> $backupFile
        cd $fullPath && rm -v $foundFiles >> $backupFile 2>&1
        echo -e "############# Files removed successfully ##############\n" >> $backupFile
}
testIt(){
        echo "Your domain name is: $domainName"
        echo "Your domain path is: $domainsPath$domainName"
        echo "Log files are at: $fullPath"
        echo "Master log file name: $logFile"
        echo "Only the files after this time will be processed: $rotate"
        echo "The date format is: $today"
        echo "Path to store compressed file: $oldLogs"
        echo "First File to be prossed: $(echo "$foundFiles" | head -1)"
        echo "Total files to be processed: $(echo "$foundFiles" | wc -w)"
        echo "Listing: "${foundFiles}""
        echo "Last file to be processed: $(echo "$foundFiles" | tail -1)"
}

########################## Switch Mode #######################################

        case "$mode" in
                "delete" | "D" | "d" | "1" )
        directDelete ;;
                "test" | "H" | "h" | "2" )
        testIt ;;
                "notimplemented" | "na" | "naa" | "7" )
        esac

############################################################################
#alert-server  banksmart  batch-job  batch-sms  iso  jdk  job-processor  notification-server  notification-server-STANDALONE  sms-core
