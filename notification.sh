#!/bin/bash
#copy the PATH env. variable here.(echo $PATH or env)
PATH=/home/sagar/perl5/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
######
#let script exit if a command fails. failsafe
set -o errexit
#let script exit if an unsed variable is used. failsafe
set -o nounset
readonly notificationPath="/home/sagar/backup/"    #Plz include / at last 
readonly logFile="Master.log"                                              #Master log file name
today=$('date' '+%Y-%m-%d@%H:%M:%S') # (: gives issue in compression use: --force-local)
action=1             #Please pass argument as 1=server log,2=app log,3=both log            
readonly notificationDir=(alert-processor auto-registration cbs-alert)    #domain name clean | don't include /  like:--(admin client ips report)
for notificationName in "${notificationDir[@]}"
do
notificationLog(){
		backPrefix="wrapper.Log_"
		rotate="1"                               #After the defined day's files will be processed(days)
		mode="4"                                #Backup->Backup or Delete->delete files etc..
		logPrefix="wrapper.log_"            #Prefix of application log that differenciate between active log
		fullPath=$notificationPath$notificationName/logs
		mkdir -p "${fullPath}"
		foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name "${logPrefix}*" -mtime +$rotate)
		backupFile=${fullPath}/${logFile}
		oldLogs=$fullPath/oldLogs
		echo first hit
	}
notificationLog
#applicationLog(){
#		backPrefix="applicationLog_"
#		rotate="1"                               #After the defined day's files will be processed(days)
#		mode="3"                                #Backup->Backup or Delete->delete files etc..
#		logPrefix="application.log."            #Prefix of application log that differenciate between active log
#		fullPath=$notificationPath$domainName/logs/application
#		mkdir -p "${fullPath}"
#		foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name "${logPrefix}*" -mtime +$rotate)
#		backupFile=${fullPath}/${logFile}
#		oldLogs=$fullPath/oldLogs
#}
#	if [[ -z "$action" ]] || [ "$action" -lt 1 -o "$action" -gt 4 ]; then
#		echo "Umm, Either argument not supplied or is out of range";
#		exit 0;
#	fi
#	if [ "${action}" = "1" ] || [ "${action}" = "3" ];then
#		serverLog
#	fi
#	if [ "${action}" = "2" ];then
#		applicationLog
#	fi
	# exit after server file not found and repeat function call resolved after return...
	files(){
		if [ "$foundFiles" = "" ]; then
			echo "##################### ${today} ###################" >> ${backupFile}
			echo "No older then $rotate days files found" >> $backupFile
			echo "#############################################################" >> $backupFile
			return 1
		else
			return 0
		fi
	}
	domainCheck(){
		if [ ! -d $fullPath ]; then
			echo "#######################################";
			echo "Domain dosen't seems to be exists";
			echo "#######################################";
			exit;
		fi
	}
	domainCheck
	#fileCheck
	mkdir -p "${oldLogs}"
	directDelete(){
		echo "############# Delete@$today ###############" >> $backupFile
		cd $fullPath && rm -v $foundFiles >> $backupFile 2>&1
		echo -e "######### Files removed successfully ########\n" >> $backupFile
	}
	moveIt(){
		echo "############# Backup@$today ###############" >> $backupFile
		cd $fullPath && mv -fv $foundFiles oldLogs >> $backupFile 2>&1
		echo -e "############ Files backuped successfully ############\n" >> $backupFile
	}
	compressIt(){
		echo "############## Compressing following files on $today ##############" >> $backupFile 2>&1
		cd "${oldLogs}" && tar cfvz "$backPrefix""$today".tar.gz "${logPrefix}"* --force-local --remove-files >>$backupFile 2>&1
		echo "############## Compression and Removal Finished ###########################################" >> $backupFile 2>&1
	}
	moveCompress(){
		#if files;then
		moveIt          #if not found new file compression didn't happen
		compressIt
		#return 0
	#else
	#	continue 2
	#fi
	}
	echo i am here
	testIt(){
		echo "Your domain name is: $notificationName"
		echo "Your domain path is: $notificationPath$notificationName"
		echo "Log files are at: $fullPath"
		echo "Master log file name: $logFile"
		echo "Only the files after this time will be processed: $rotate"
		echo "The day format is: $today"
		echo "This is the path where compressaed file will be stored: $oldLogs"
		echo "First File to be prossed: $(echo "$foundFiles" | head -1)"
		echo "Total files to be processed: $(echo "$foundFiles" | wc -w)"
		echo "Last file to be processed: $(echo "$foundFiles" | tail -1)"
	}
	########################## Switch Mode #######################################
	case "$mode" in
		"backup" | "B" | "b" | "1" )
			moveIt ;;
		"delete" | "D" | "d" | "2" )
			directDelete ;;
		"compress" | "C" | "c" | "3" )
			moveCompress ;;
		"test" | "H" | "h" | "4" )
			testIt ;;
		"notimplemented" | "na" | "naa" | "7" )
	esac
done
#set -x
echo i am here to
if [ "${action}" = "3" ];then
	unset backPrefix
	unset rotate
	unset mode
	unset logPrefix
	unset fullPath
	unset foundFiles
	unset backupFile
	unset oldLogs
	unset action
	$0 2
fi
#set +x
#############################################################################
