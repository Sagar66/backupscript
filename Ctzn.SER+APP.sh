#!/bin/bash
#copy the PATH env. variable here.(echo $PATH or env)
PATH=/home/sagar/perl5/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
######
#let script exit if a command fails. failsafe
set -o errexit
#let script exit if an unsed variable is used. failsafe
set -o nounset
readonly domainsPath=/home/expressbank/expressbank/glassfish/domains/    #Plz include / at last
readonly domainName="domain1"                                          #domain name clean | don't include /
readonly logFile="Master.log"                                              #Master log file name
today=$('date' '+%Y-%m-%d@%H:%M:%S') # (: gives issue in compression use: --force-local)
action="$1"             #Please pass argument as 1=server log,2=app log,3=both log            
serverLog(){
        backPrefix="serverLog_"
        rotate="1"                               #After the defined day's files will be processed(days)
        mode="2"                                #Backup->Backup or Delete->delete files etc..
        logPrefix="server.log_"            #Prefix of application log that differenciate between active log
        fullPath=$domainsPath$domainName/logs
        foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name "${logPrefix}*" -mtime +$rotate)
        backupFile=${fullPath}/${logFile}
        oldLogs=$fullPath/oldLogs
        echo first hit
}
applicationLog(){
        backPrefix="applicationLog_"
        rotate="1"                               #After the defined day's files will be processed(days)
        mode="3"                                #Backup->Backup or Delete->delete files etc..
        logPrefix="application.log."            #Prefix of application log that differenciate between active log
        fullPath=$domainsPath$domainName/logs/application
        foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name "${logPrefix}*" -mtime +$rotate)
        backupFile=${fullPath}/${logFile}
        oldLogs=$fullPath/oldLogs
        echo second hit
}
if [ "${action}" = "1" ] || [ "${action}" = "3" ];then
        serverLog
fi
if [ "${action}" = "2" ];then
        applicationLog
fi

files(){
        if [ "$foundFiles" = "" ]; then
                echo "##################### ${today} ###################" >> ${backupFile}
                echo "No older then $rotate days files found" >> $backupFile
                echo "#############################################################" >> $backupFile
                exit;
        fi
}
fileCheck(){
        if [ ! -e $backupFile ]; then
                touch $backupFile
        fi
}
domainCheck(){
        if [ ! -d $fullPath ]; then
                echo "#######################################";
                echo "Domain dosen't seems to be exists";
                echo "#######################################";
                exit;
        fi
}
domainCheck
fileCheck
mkdir -p $fullPath/oldLogs
directDelete(){
        files
        echo "############# Delete@$today ###############" >> $backupFile
        cd $fullPath && rm -v $foundFiles >> $backupFile 2>&1
        echo -e "######### Files removed successfully ########\n" >> $backupFile
}
moveIt(){
        files
        echo "############# Backup@$today ###############" >> $backupFile
        cd $fullPath && mv -fv $foundFiles oldLogs >> $backupFile 2>&1
        echo -e "############ Files backuped successfully ############\n" >> $backupFile
}
compressIt(){
        echo "############## Compressing following files on $today ##############" >> $backupFile 2>&1
        cd "${oldLogs}" && tar cfvz "$backPrefix""$today".tar.gz "${logPrefix}"* --force-local --remove-files >>$backupFile 2>&1
        echo "############## Compression and Removal Finished ###########################################" >> $backupFile 2>&1
}
moveCompress(){
        moveIt          #if not found new file compression didn't happen
        compressIt
}
testIt(){
        echo "Your domain name is: $domainName"
        echo "Your domain path is: $domainsPath$domainName"
        echo "Log files are at: $fullPath"
        echo "Master log file name: $logFile"
        echo "Only the files after this time will be processed: $rotate"
        echo "The day format is: $today"
        echo "This is the path where compressaed file will be stored: $oldLogs"
        echo "First File to be prossed: $(echo "$foundFiles" | head -1)"
        echo "Total files to be processed: $(echo "$foundFiles" | wc -w)"
        echo "Last file to be processed: $(echo "$foundFiles" | tail -1)"
}
########################## Switch Mode #######################################
        case "$mode" in
        "backup" | "B" | "b" | "1" )
                moveIt ;;
        "delete" | "D" | "d" | "2" )
                directDelete ;;
        "compress" | "C" | "c" | "3" )
                moveCompress ;;
        "test" | "H" | "h" | "4" )
                testIt ;;
        "notimplemented" | "na" | "naa" | "7" )
        esac
if [ "${action}" = "3" ];then
        unset backPrefix
        unset rotate
        unset mode
        unset logPrefix
        unset fullPath
        unset foundFiles
        unset backupFile
        unset oldLogs
        unset action
        $0 2
fi
#############################################################################