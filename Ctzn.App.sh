#!/bin/bash
#Please modify carefully if you wish to!!!
#For CTZN.
##sagar.khatiwada@f1soft.comi
set -o nounset
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/cpanel/composer/bin:/root/bin:/opt/cpanel/composer/bin
################## Local VAR Group (SET VAR'S CAREFULLY) ###################
domainsPath=/home/expressbank/expressbank/glassfish/domains/    #Plz include / at last
domainName="domain1"                                          #domain name clean | don't include /
rotate="1"                               #After the defined day's files will be processed(days)
logFile=Master.log                                              #Master log file name
mode="1"                                #Backup->Backup or Delete->delete files etc..
################ +Globals Don't Change ##################################
today=$('date' '+%Y-%m-%d@%H:%M:%S') # (: gives issue in compression use: --force-local)
logPrefix="application.log."
fullPath=$domainsPath$domainName/logs/application
foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name "${logPrefix}*" -mtime +$rotate)
backupFile=${fullPath}/${logFile}
oldLogs=$fullPath/oldLogs
################## Functions ################################
files(){
        if [ "$foundFiles" = "" ]; then
                echo "##################### ${today} ###################" >> ${backupFile}
                echo "No older then $rotate days files found" >> $backupFile
                echo "#############################################################" >> $backupFile
                exit;
        fi
}
fileCheck(){
        if [ ! -e $backupFile ]; then
                touch $backupFile
        fi
}
domainCheck(){
        if [ ! -d $fullPath ]; then
                echo "#######################################";
                echo "Domain dosen't seems to be exists";
                echo "#######################################";
                exit;
        fi
}
domainCheck
fileCheck
mkdir -p $fullPath/oldLogs
set -o errexit
moveIt(){
        files
        echo "############# Backup@$today ###############" >> $backupFile
        cd "$fullPath" && mv -fv $foundFiles oldLogs >> $backupFile 2>&1
        echo -e "############ Files backuped successfully ############\n" >> $backupFile
}
compressIt(){
        echo "############## Compressing following files on $today ##############" >> $backupFile 2>&1
        cd "$oldLogs" && tar cfvz applicationLog_"$today".tar.gz "${logPrefix}"* --force-local --remove-files >>$backupFile 2>&1
        echo "############## Compression and Removal Finished ###########################################" >> $backupFile 2>&1
}
moveCompress(){
        moveIt          #if not found new file compression didn't happen
        compressIt
}
testIt(){
        echo "Your domain name is: $domainName"
        echo "Your domain path is: $domainsPath$domainName"
        echo "Log files are at: $fullPath"
        echo "Master log file name: $logFile"
        echo "Only the files after this time will be processed: $rotate"
        echo "The day format is: $today"
        echo "This is the path where compressaed file will be stored: $oldLogs"
        echo "First File to be prossed: $(echo "$foundFiles" | head -1)"
        echo "Total files to be processed: $(echo "$foundFiles" | wc -w)"
        #echo "${foundFiles}";
        echo "Last file to be processed: $(echo "$foundFiles" | tail -1)"
}
########################## Switch Mode #######################################
        case "$mode" in
                "compress" | "C" | "c" | "1" )
        moveCompress ;;
                "test" | "H" | "h" | "2" )
        testIt ;;
                "notimplemented" | "na" | "naa" | "7" )
        esac
############################################################################
