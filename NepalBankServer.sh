#!/bin/bash
#Following script is used for mode=3(moveCompres) server logs older then 30 days in 2 days interval
#Please modify carefully if you wish to!!!
#Other modes are not verified!!!
#For NepalBank LTD.
##sagar.khatiwada@f1soft.com
################## Local VAR Group ###################
domainsPath=/home/expressbank/expressbank/glassfish/domains/    #Plz include / in last
domainName=expressbank                                          #domain name clean
rotate=30                               #After the defined day the files will be processed(days)
logFile=Master.log                                              #Master log file
mode="3"                                #Backup->Backup or delete->delete files
deleteAfter=12                                                  #if delete function is used(days)
################ +Globals Don't Change ##################################
today=$('date' '+%Y-%m-%d@%H:%M:%S') # (: gives issue in compression use: --force-local)
fullPath=$domainsPath$domainName/logs
#findFiles=`find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate`
foundFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate)
#echo $foundFiles
#exit;
freeDisk=$(df | awk 'FNR == 2 {print $(NF-2)}')
backupFile=${fullPath}/${logFile}
limit=$(($deleteAfter * 1048576))
oldLogs=$fullPath/oldLogs
################## Functions ################################
range=1                                 #File between this number of days were compressed
lastFile=$(cd $fullPath && ls -t server.log_* 2>/dev/null | tail -1)
lastDate=$(cd $fullPath && date -r "${lastFile}" '+%Y-%m-%d' 2>/dev/null)
prevDate=$(cd $fullPath && date -d "$lastDate+$range days" '+%Y-%m-%d')
rangeFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate -newermt "$lastDate 00:00:01" ! -newermt "$prevDate 00:00:00")
###########PARK
###########
files(){
        if [ "$foundFiles" = "" ]; then
                echo "##################### ${today} ###################" >> ${backupFile}
                echo "No older then $rotate days files found" >> $backupFile
                echo "#############################################################" >> $backupFile
                exit;
        fi
}
fileCheck(){
        if [ ! -e $backupFile ]; then
                touch $backupFile
        fi
}
domainCheck(){
        if [ ! -d $fullPath ]; then
                echo "#######################################";
                echo "Domain dosen't seems to be exists";
                echo "#######################################";
                exit;
        fi
}
domainCheck
fileCheck
mkdir -p $fullPath/oldLogs
moveIt(){
        files
        echo "############# Backup@$today ###############" >> $backupFile
        cd $fullPath && mv -fv $foundFiles oldLogs >> $backupFile 2>&1
        echo -e "############Files backuped successfully############\n" >> $backupFile
}
directDelete(){
        files
        echo "############# Delete@$today ###############" >> $backupFile
        cd $fullPath && rm -fv "${foundFiles}" >> $backupFile 2>&1
        echo -e "######### Files removed successfully ########\n" >> $backupFile
        exit;
}
compressIt(){
        echo "############## Compressing following files on $today ##############" >> $backupFile 2>&1
        cd "${oldLogs}" && tar cfvz serverLog_"${today}".tar.gz server.log_* --force-local >> $backupFile 2>&1 && rm -fv server.log_* >> $backupFile 2>&1
        echo "############## Compression and Removal Finished ###########################################" >> $backupFile 2>&1
}
moveCompress(){
        moveIt          #if not found new file compression didn't happen
        compressIt
}
deleteCompressed(){
        echo "################ Deleting Following files on $today ###############" >> $backupFile
        while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${limit}" ]
        do
                ls -t ./*.tar.gz > /dev/null 2>&1
                if [ $? -gt 0 ]; then
                        echo "There is no more files to be deleted."  >> $backupFile
                        break;
                fi
                cd $oldLogs && rm -fv "{ls -t *.tar.gz | tail -1}" >> $backupFile
                done
                echo -e "###########################################################/n" >> ${backupFile}
}
timeRange(){
        if [[ -n $rangeFiles ]]; then #If variable is not empty
        while [[ -n "$rangeFiles" ]]    #while variable is not empty.-z is for empty
        do
                count=""
                echo "################ Moving File from $lastDate to $prevDate ############" >> $backupFile
                cd $fullPath && mv -fv "${rangeFiles}" oldLogs >> $backupFile 2>&1
                echo "################ Compressing files $lastDate to $prevDate ###########" >> $backupFile
                cd $oldLogs && tar cfvz serverLog_"${lastDate}"-"${prevDate}""${count}".tar.gz "${rangeFiles}" --remove-files 1>>$backupFile 2>&1
                lastFile=$(cd $fullPath && ls -t server.log_* 2>/dev/null | tail -1)
                lastDate=$(cd $fullPath && date -r "${lastFile}" '+%Y-%m-%d' 2>/dev/null)
                prevDate=$(cd $fullPath && date -d "$lastDate+$range days" '+%Y-%m-%d')
                rangeFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate -newermt "$lastDate 00:00:01" ! -newermt "$prevDate 00:00:00")
                count=$(("${count}" + 1))
        done
        echo "########################################################################################" >>$backupFile
else
        while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${limit}" ]
        do
                echo "################ Deleting Following files on $today ###############" >> $backupFile
                cd $oldLogs && ls -t ./*.tar.gz > /dev/null 2>&1
                if [ $? -gt 0 ]; then
                        echo "There is no more files to be deleted."  >> $backupFile
                        break;
                fi
                cd $oldLogs && rm -fv "$(ls -t ./*.tar.gz | tail -1)" >> $backupFile
                echo -e "###################### Done #############################\n" >> $backupFile
        done
        echo "################ Nothing to do $today ###########" >> $backupFile
fi
}
testIt(){
        echo "Your domain name is: $domainName"
        echo "Your domain path is: $domainsPath$domainName"
        echo "Log files are at: $fullPath"
        echo "Master log file name: $logFile"
        echo "Minimum free space requirement is: $deleteAfter"
        echo "Only the files after this time will be processed: $rotate"
        echo "The day format is: $today"
        echo "Your current free disk is: $freeDisk"
        echo "This is the path where compressaed file will be stored: $oldLogs"
        echo "First File to be prossed: $(echo "$foundFiles" | head -1)"
        echo "Total files to be processed: $(echo "$foundFiles" | wc -l)"
        echo "Last file to be processed: $(echo "$foundFiles" | tail -1)"
        echo "Limit Set: $limit"
}

########################## Switch Mode #######################################

        case "$mode" in
                "backup" | "B" | "b" | "1" )
        moveIt ;;
                "delete" | "D" | "d" | "2" )
        directDelete ;;
                "compress" | "C" | "c" | "3" )
        moveCompress ;;
                "mix" | "M" | "m" | "4" )
        deleteCompressed ;;
                "time" | "T" | "t" | "5" )
        timeRange ;;
                "test" | "H" | "h" | "6" )
        testIt ;;
                "notimplemented" | "na" | "naa" | "7" )
        esac

############################################################################