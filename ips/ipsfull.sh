#!/bin/bash
#================================================================
# HEADER
#================================================================
#% SYNOPSIS
#+    ${SCRIPT_NAME}
#%
#% DESCRIPTION
#%    This is a script to full branch list and bank list from Live or UAT IPS API..
#%    This script pulls realtime lists and makes insert query for IPS middleware.
#%
#% OPTIONS
#% Just execute for now.
#%	!!!-OPEN FOR ANY ADVISE-!!!
#% 
#% EXAMPLES
#%  N/A
#%
#================================================================
#- IMPLEMENTATION
#-    version         ${SCRIPT_NAME} 0.0.0.3 (alpha)
#-    author          Sagar Khatiwada(sagar.khatiwada66@gmail.com)
#-    copyright       Enjoy
#-    license         N/A
#-    script_id/      0003
#-    PushCount		
#================================================================
#  HISTORY
#     2020/02/02 : sks63 : Script creation
# 
#================================================================
#  DEBUG OPTION
#    set -n  # Uncomment to check your syntax, without execution.
#    set -x  # Uncomment to debug this shell script
#    set -v  # Uncomment to debug on verbose mode
#================================================================
#  BUGS
#    -- Very few exceptions considered currently
#
#================================================================
# END_OF_HEADER
#================================================================
# BEGAIN
#================================================================
# Exit on error/un used variable check/pipe fail check
set -o errexit 
set -o nounset
set -o pipefail
#================================================================
# VARIABLES
HTTPS=0
AUTH_USER="nabilib"
AUTH_PASS="Abcd@123"
API_USER="NABILIB@999"
API_PASS="123Abcd@123"
BASE_API="http://182.93.64.252:6443"
YOUR_BANK=""

#================================================================
# INFO
echo Bsic auth credintials:- $AUTH_USER \&\& $AUTH_PASS
echo API auth credintials:- $API_USER \&\& $API_PASS
echo BASE API URI:- $BASE_API
echo

#================================================================
# Basic Token Generation
echo Generating basic token...
sleep 1
getToken(){
	TOKEN=$(echo $(echo -ne "${AUTH_USER}:${AUTH_PASS}" | base64))
	echo $TOKEN
}
echo success 
getToken
sleep 2
echo
echo Generating Access Token...

#================================================================
# ACCESS/REFRESH TOKEN 
getAccessToken(){
	ACCESS_TOKEN=$(curl -sk --location --request POST "$BASE_API/oauth/token" --header 'Content-Type: application/x-www-form-urlencoded' --header "Authorization: Basic ${TOKEN}" --data-urlencode "password=${API_PASS}" --data-urlencode 'grant_type=password' --data-urlencode "username=${API_USER}")
	echo $ACCESS_TOKEN
}
echo success
getAccessToken
sleep 2
echo
echo Clearing access token and refresh token.

#================================================================
# CLEAR TOKEN
clearToken(){
	clearAccessToken=$(echo $ACCESS_TOKEN | awk -F\" {'print $4'})
	clearRefreshToken=$(echo $ACCESS_TOKEN | awk -F\" {'print $12'})
}
echo success
echo
clearToken
echo $clearAccessToken
echo $clearRefreshToken
echo Access token is applicable for 30s only but refresh token is applicable for 12hours..
echo Access token can be re-requested using refresh token.
sleep 4
echo
echo Pulling Bank list using same token.

#================================================================
# BANK LIST PULL
getBankList(){
	BANK=$(curl -sk --location --request POST "$BASE_API/api/getbanklist" --header "Authorization: Bearer ${clearAccessToken}")
}
getBankList
echo
echo success
echo
echo $BANK
echo
echo "Storing banks";
sleep 4
echo $BANK > ./banks.list
echo Processing for bank insert query

#================================================================
# PARSE BANK LIST
bankClear(){
	echo
	sleep 2
	echo Clearing bank codes...
	echo
	cat banks.list | sed -E -e 's/"|,|\[|\]|\{//g' | sed 's/\}/\n/g' | sed 's/bankId://g' | sed 's/bankName:/,/g' | sed '/^$/d' | sed 's/^/"/g' | sed 's/$/"/g' | sed 's/,/","/g' > bankClear.list
	echo success
}
bankClear
 
#================================================================
# INSERT BANK LIST
bankInsert(){
	echo Accessing bank insert script
	sleep 1
	echo accessed \& processing
	local CURRENT_TIME=$(date +%Y-%m-%d\ %H:%M:%S)
	echo Current date format $CURRENT_TIME
	echo Creating csv for insert query...
	sleep 1
	cat bankClear.list | cat bankClear.list | sed "s/^/\"${CURRENT_TIME}\",/g" | sed "s/^/\"${CURRENT_TIME}\",/g" | sed 's/^/"Y",/g' | sed 's/$/,\"BOTH\"/g' | awk '{printf("\"%d\",%s\n", NR, $0)}' > bankClearCSV.list
	echo done
	echo Creating final query for bank insert...
	sleep 2
	cat bankClearCSV.list | sed 's/^/insert into `BANK_DUMMY` (`ID`, `ACTIVE`, `ADDED_DATE`, `LAST_MODIFIED_DATE`, `CODE`, `NAME`, `MODE`) values(/g' | sed 's/$/);/g' > finalBankInsert.sql
	echo Successful
	echo Please find final query at finalBankInsert.sql
}
bankInsert

#================================================================
# PULL BRANCH LIST
getBranchList(){
	BRANCH=$(curl -sk --location --request POST "$BASE_API/api/getbranchlist" --header "Authorization: Bearer ${clearAccessToken}")
}
getBranchList
echo $BRANCH
echo $BRANCH > ./branch.list
echo
echo Branch pull complete
echo Processing for branch insert query.

#================================================================
# CLEAR BRANCH LIST
branchClear(){
	echo clearing branch list
	cat branch.list | sed -E -e 's/"|,|\[|\]|\{//g' | sed 's/\}/\n/g' |  sed '/^$/d' | sed -E 's/branchId:|$/"/g' | sed -E 's/bankId:|branchName:/\",\"/g' > branchClear.list
}
branchClear

#================================================================
# INSERT BRANCH LIST
branchInsert(){
	local CURRENT_TIME=$(date +%Y-%m-%d\ %H:%M:%S)
	echo Current date format $CURRENT_TIME
	echo Creating csv for branch insert query...
	sleep 1
	cat branchClear.list | sed "s/^/\"${CURRENT_TIME}\",/g" | sed "s/^/\"${CURRENT_TIME}\",/g" | sed 's/^/"Y",/g' | awk '{printf("\"%d\",%s\n", NR, $0)}' > branchClearAll.list
	echo done
	echo Maintaining bankId to ID relation.
	echo Getting bank codes
	cat bankClear.list | awk -F\" '{print $(NF-3)}' | awk '{print NR  "\",\"" $s}' | sed -E 's/^|$/\"/g' > bankCodes.list
	#awk '{print NR  "> " $s}' bankCodes.list > bankCodes.list
	#awk '{print NR  "," $s}' bankCodes.list > tmp && mv tmp bankCodes.list
	#awk -F\" 'BEGIN {OFS = FS} FNR==NR{a[$4]=$2;next}{$12=a[$12];print}' bankCodes.list branchClearAll.list > branchClearCSV.list
	echo Checking if codes are repeated...
	local BANK_CODES=$(cat bankCodes.list | awk -F\" {'print $4'} | uniq -cd)
if [ -z "$BANK_CODES" ];then
		echo "No repeated bank code found"
	else
	        echo "following are the repeated codes"
	        cat bankCodes.list | awk -F\" {'print $4'} | uniq -cd
	        echo Please maintain uniqueness and continue
	        exit
	        #cat bankCodes.list | awk -F\" '{print $(NF-3)}'
fi
	sleep 1
	echo Replacing updates
	awk -F\" 'BEGIN {OFS = FS} FNR==NR{a[$4]=$2;next}{$12=a[$12];print}' bankCodes.list branchClearAll.list > branchClearData.list
	#Missing Bank Codes
	local MISSED=$(awk -F\" 'BEGIN {OFS = FS} $11 && !$12{ $12="MISSED" }1' branchClearData.list | grep -c "MISSED")
if [ -z "$MISSED" ];then
		echo
		echo "No missing bank codes";
	else
		echo
		echo "$MISSED missed bank codes found"
		echo Setting "12345" as those missed bank codes.
		echo
		sleep 4
		awk -F\" 'BEGIN {OFS = FS} $11 && !$12{ $12="12345" }1' branchClearData.list | grep "12345"
		awk -F\" 'BEGIN {OFS = FS} $11 && !$12{ $12="12345" }1' branchClearData.list > branchClearCSV.list
		echo
		local REPLACED=$(awk -F\" 'BEGIN {OFS = FS} $11 && !$12{ $12="12345" }4' branchClearData.list | grep -c "12345")
		echo "$REPLACED missing bank codes are replaced"
		[[ "$MISSED" -eq "$REPLACED"  ]] && echo Missing and Replaced codes matched. || exit
		echo 
		echo Bank codes that are listed in branch API but not in Bank API are.
		echo
		cat branchClear.list | awk -F\" '{print $4}' | sort | uniq > bankCodes.branch
		cat bankClear.list | awk -F\" '{print $2}' > bankCodes.bank
		echo There are $(awk -F\" 'FNR==NR{a[$1,$2]=1; next}  !a[$1,$2]' bankCodes.bank bankCodes.branch | wc -l) missing bank codes
		echo
		echo Following are missed bank codes in bank API.
		sleep 2
		echo
		echo -e "Missed Bank Codes:\n" > missedBankCodes.list
		awk -F\" 'BEGIN { ORS=" " }; FNR==NR{a[$1,$2]=1; next}  !a[$1,$2]' bankCodes.bank bankCodes.branch
		awk -F\" 'BEGIN { ORS=" " }; FNR==NR{a[$1,$2]=1; next}  !a[$1,$2]' bankCodes.bank bankCodes.branch >> missedBankCodes.list
		sleep 5
		echo
	       	echo -e "Relations:\n"	
		echo -e "\n">> missedBankCodes.list
		echo Following are branchId to branchName relation with missed bankCodes.
		echo
		co=$(awk -F\" 'FNR==NR{a[$1,$2]=1; next}  !a[$1,$2]' bankCodes.bank bankCodes.branch);echo "${co[@]}" | while read line; do grep "$line" branchClear.list && grep "$line" branchClear.list >> missedBankCodes.list; done	
		echo
		sleep 1
fi
	#while read line;do
	#       CODE=$(grep -n $line bankCodes.list | awk -F: '{print $1}')
	#       cat branchClearCSV.list | awk -F\" '{print $(NF-3)}' | sed -i "s/${line}/${CODE}/g"
	#done < bankCodes.list
	echo success
	sleep 2
	echo Creating final query for bank insert...
	sleep 2
	cat branchClearCSV.list | sed 's/^/insert into `BRANCH_DUMMY` (`ID`, `ACTIVE`, `ADDED_DATE`, `LAST_MODIFIED_DATE`, `CODE`, `BANK_ID`, `NAME`) values(/g' | sed 's/$/);/g' > finalBranchInsert.sql
	echo Successful
}
branchInsert
sleep 1
echo removing temp files
rm banks.list branch.list bankClear.list bankClearCSV.list branchClear.list bankCodes.list branchClearAll.list branchClearCSV.list branchClearData.list bankCodes.branch bankCodes.bank
echo
echo All Done
echo
echo Please find final query at finalBankInsert.sql \& finalBranchInsert.sql
echo
echo Missing bank codes are listed in missedBankCodes.list
echo Inserting into DB
#mysql -u expressbank -pexpressbank IPS1 BANK_DUMMY < finalBankInsert.sql
#mysql -u expressbank -pexpressbank IPS1 BRANCH_DUMMY < finalBranchInsert.sql
