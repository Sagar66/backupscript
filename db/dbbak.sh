#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail
PATH="/home/sagar/perl5/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/mnt/c/WINDOWS/system32:/mnt/c/WINDOWS:/mnt/c/WINDOWS/System32/Wbem:/mnt/c/WINDOWS/System32/WindowsPowerShell/v1.0/:/mnt/c/Program Files (x86)/Intel/OpenCL SDK/2.0/bin/x86:/mnt/c/Program Files (x86)/Intel/OpenCL SDK/2.0/bin/x64:/mnt/c/Program Files/Microsoft SQL Server/120/DTS/Binn/:/mnt/c/Program Files/Microsoft SQL Server/Client SDK/ODBC/110/Tools/Binn/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/Tools/Binn/:/mnt/c/Program Files/Microsoft SQL Server/120/Tools/Binn/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/Tools/Binn/ManagementStudio/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/DTS/Binn/:/mnt/c/WINDOWS/System32/OpenSSH/:/mnt/c/Program Files/PuTTY/:/mnt/c/Program Files/Java/jdk1.7.0_80/bin:/mnt/c/Program Files/Intel/WiFi/bin/:/mnt/c/Program Files/Common Files/Intel/WirelessCommon/:/mnt/c/Program Files/ISC BIND 9/bin:/mnt/c/Users/milan.kc/AppData/Local/Microsoft/WindowsApps:/mnt/c/Program Files (x86)/SSH Communications Security/SSH Secure Shell:/mnt/c/adb:/mnt/c/WINDOWS/system32:/mnt/c/WINDOWS:/mnt/c/WINDOWS/System32/Wbem:/mnt/c/WINDOWS/System32/WindowsPowerShell/v1.0/:/mnt/c/WINDOWS/System32/OpenSSH/:/mnt/c/Program Files (x86)/Bitvise SSH Client:/mnt/c/Users/sagar.khatiwada/AppData/Local/Microsoft/WindowsApps:/snap/bin"
ABSOLUTE_PATH=$( cd "$(dirname "$0")" ; pwd -P )
USER="bankex"
PASSWORD="bankex"
OUTPUTDIR="./DB_DUMPS"
MASTER_LOG="$ABSOLUTE_PATH"/Master.log
DELETE_AFTER="1"
DATABASES=(EXPRESSBANK IPS)
echo "################################ Action taken at $(date +%Y-%m-%d@%H:%M:%S) ###############################################" >> ./Master.log
if [ ! -d "$OUTPUTDIR" ]; then
	mkdir -p "$OUTPUTDIR";
fi
for db in "${DATABASES[@]}"
do
	mysqldump --force --single-transaction --opt -u "$USER" -p"$PASSWORD" --databases "$db" | gzip > ""$OUTPUTDIR"/"$db"/"$db"_$(date +%Y-%m-%d@%H:%M:%S).sql.gz"
	FILES=$(cd "$OUTPUTDIR"/"$db" && find . -maxdepth 1 -type f -name "${db}*" -mtime +$DELETE_AFTER | tail -1) 2>> ./Master.log
	removeFile(){
		echo ""$db" Backup completed" >> ./Master.log
		if [ "$FILES" == "" ];then
			echo "No files to delete" >> ./Master.log 2>&1
		else
			echo "These are the files to be deleted:- $FILES" >> ./Master.log 
			cd "$OUTPUTDIR"/"$db" && rm -fv $FILES >> $MASTER_LOG 2>&1
			cd "$ABSOLUTE_PATH"
		fi
	}
	removeFile
done
echo "################################ Done Done Done ###############################################" >> ./Master.log

