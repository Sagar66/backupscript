#!/bin/bash
##working
##ssh-keygen -t rsa
PATH="/home/sagar/perl5/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/mnt/c/WINDOWS/system32:/mnt/c/WINDOWS:/mnt/c/WINDOWS/System32/Wbem:/mnt/c/WINDOWS/System32/WindowsPowerShell/v1.0/:/mnt/c/Program Files (x86)/Intel/OpenCL SDK/2.0/bin/x86:/mnt/c/Program Files (x86)/Intel/OpenCL SDK/2.0/bin/x64:/mnt/c/Program Files/Microsoft SQL Server/120/DTS/Binn/:/mnt/c/Program Files/Microsoft SQL Server/Client SDK/ODBC/110/Tools/Binn/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/Tools/Binn/:/mnt/c/Program Files/Microsoft SQL Server/120/Tools/Binn/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/Tools/Binn/ManagementStudio/:/mnt/c/Program Files (x86)/Microsoft SQL Server/120/DTS/Binn/:/mnt/c/WINDOWS/System32/OpenSSH/:/mnt/c/Program Files/PuTTY/:/mnt/c/Program Files/Java/jdk1.7.0_80/bin:/mnt/c/Program Files/Intel/WiFi/bin/:/mnt/c/Program Files/Common Files/Intel/WirelessCommon/:/mnt/c/Program Files/ISC BIND 9/bin:/mnt/c/Users/milan.kc/AppData/Local/Microsoft/WindowsApps:/mnt/c/Program Files (x86)/SSH Communications Security/SSH Secure Shell:/mnt/c/adb:/mnt/c/WINDOWS/system32:/mnt/c/WINDOWS:/mnt/c/WINDOWS/System32/Wbem:/mnt/c/WINDOWS/System32/WindowsPowerShell/v1.0/:/mnt/c/WINDOWS/System32/OpenSSH/:/mnt/c/Users/sagar.khatiwada/AppData/Local/Microsoft/WindowsApps:/snap/bin"
#NFS
USER_NAME="sagar"
PASSWORD="passcode"
HOST="localhost"
PORT="22"
REMOTE_PATH="/home/sagar/backup/db/REMOTE/asa"			#Please Provide absolute path
REMOTE_VERIFY="FALSE"
PROTOCOL=""
#### Local Files
ROTATE="1"
LOCAL_PATH="/home/sagar/backup/db/LOCAL"			#Please provide absolute path
FILE_PREFIX='LOG'
MODE=""
DELETE_AFTER="16"
TODAY=$('date' '+%Y-%m-%d@%H:%M:%S')
BACKUP_FILE="Master.log"
## Mount
MOUNT_PATH=
FILE_PER=1
########################
########################
FREE_DISK=$(df | awk 'FNR == 2 {print $(NF-2)}')
echo Free disk= $FREE_DISK
#BACKUP_FILE=${fullPath}/${logFile}
LIMIT=$(("$DELETE_AFTER" * 1048576))
echo Free Disk= $(("$FREE_DISK"/1024/1024)) GB
echo Limit= $LIMIT
#exit;
#########################
RANGE=""
LAST_FILE=$(cd $LOCAL_PATH && ls -t "${FILE_PREFIX}"* 2>/dev/null | tail -1)
if [ "$LAST_FILE" == "" ];then
	echo "File couldn't be found";
	exit 0;
fi
echo Last File= $LAST_FILE
LAST_DATE=`cd $LOCAL_PATH && date -r $LAST_FILE '+%Y-%m-%d' 2>/dev/null`
echo Last Date= $LAST_DATE
PREV_DATE=`cd $LOCAL_PATH && date -d "$LATS_DATE+$RANGE days" '+%Y-%m-%d'`
echo Prev Date= $PREV_DATE
RANGE_FILES=`cd $LOCAL_PATH && find . -maxdepth 1 -type f -name "${FILE_PREFIX}*" -mtime +"$ROTATE" -newermt "$LAST_DATE 00:00:01" ! -newermt "$PREV_DATE 00:00:00"`
echo Found Files= $RANGE_FILES
#########################
##### Functions #########
sshAutoEngine(){
	SSH_TEMP=./temp.file
cat > ${SSH_TEMP} <<EOL
echo "${PASSWORD}"
EOL
	chmod u+x ${SSH_TEMP}
	export DISPLAY=:0
	export SSH_ASKPASS=${SSH_TEMP}
	# LogLevel error is to suppress the hosts warning. The others are
	# necessary if working with development servers with self-signed
	# certificates.
	#SSH_OPTIONS="-oLogLevel=error"
	#SSH_OPTIONS="${SSH_OPTIONS} -oStrictHostKeyChecking=no"
	#SSH_OPTIONS="${SSH_OPTIONS} -oUserKnownHostsFile=/dev/null"

	#echo "echo passcode" > ./ssh
	#chmod 777 ./ssh
	#export SSH_ASKPASS="./ssh"
	#export DISPLAY=LOGIN
	#setsid ssh "$USER_NAME"@"$HOST" -p"$PORT"
	#rm ./ssh
}

scpSend(){
	#scp $SEND_FILES $USER_NAME@$HOST:$REMOTE_PATH
	echo "################ SCP Backup on $TODAY ###############" #>> $BACKUP_FILE
	while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${LIMIT}" ]
	do
		cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* > /dev/null 2>&1
		if [ $? -gt 0 ]; then
			echo "There is no more files to be deleted."  #>> $BACKUP_FILE
			break;
		fi
		echo There are files to be moved;
		echo This is the next file to be moved;
		local AC_FILE=$(cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* | tail -"$FILE_PER")
		echo $AC_FILE
		if [ "$REMOTE_VERIFY" == "FALSE" ];then
			sshAutoEngine && setsid ssh ${USER_NAME}@${HOST} "mkdir -p ${REMOTE_PATH}"
			echo kera $?
			if [ "$?" -gt 0 ];then
				echo "Something Wrong with connection to remote server"
				exit;
			else
				REMOTE_VERIFY="TRUE"
			fi
		fi
		sshAutoEngine && setsid ssh ${USER_NAME}@${HOST} 'ls ${REMOTE_PATH}/${AC_FILE} && [[ "$?" -eq 0  ]] && {echo "Files exits on remote host, so no further action taken"; exit 2;} || echo -e "File dosent exists continue"'
		cd "$LOCAL_PATH" && sshAutoEngine && setsid scp -P"$PORT" $(ls -t "$FILE_PREFIX"* | tail -1) "$USER_NAME"@"$HOST":"$REMOTE_PATH"
		echo is exit status $?
		if [ "$?" -gt 0 ];then
			echo something error
			exit;
		fi
	done
	echo "Storage is under limit:(No files need to be deleted)"
	echo -e "###########################################################/n" ##>> ${BACKUP_FILE}
}
scpSend
exit;
localSend(){
	echo "################ Local Backup on $TODAY ###############" >> $BACKUP_FILE
	while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${LIMIT}" ]
	do
		cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* > /dev/null 2>&1
		if [ $? -gt 0 ]; then
			echo "There is no more files to be deleted."  >> $BACKUP_FILE
			break;
		fi
		echo There are files to be moved;
		echo These are the next file to be moved;
		local AC_FILE=$(cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* | tail -"$FILE_PER")
		echo $AC_FILE
		#something

	done
	echo "Storage is under limit:(No files need to be deleted)"
	echo -e "###########################################################/n" ##>> ${BACKUP_FILE}
}
sendTest(){
	cd $LOCAL_PATH
}
deleteCompressed(){
	echo "################ Deleting Following files on $TODAY ###############" #>> $BACKUP_FILE
	while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${LIMIT}" ]
	do
		cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* > /dev/null 2>&1
		if [ $? -gt 0 ]; then
			echo "There is no more files to be deleted."  #>> $BACKUP_FILE
			break;
		fi
		echo There are files to be deleted;
		echo This is the next file to be moved
		cd "$LOCAL_PATH" && ls -t "$FILE_PREFIX"* | tail -1
		cd "$LOCAL_PATH" && mv $(ls -t "$FILE_PREFIX"* | tail -1) ../REMOTE
		cd "$REMOTE_PATH" && rm -fv $(ls -t "$FILE_PREFIX"* | tail -1) #>> $BACKUP_FILE
	done
	echo "Storage is under limit:(No files need to be deleted)"
	echo -e "###########################################################/n" ##>> ${BACKUP_FILE}
}
#deleteCompressed
timeRange(){
	if [[ -n $rangeFiles ]]; then #If variable is not empty
		while [[ -n "$rangeFiles" ]]    #while variable is not empty.-z is for empty
		do
			local count=""
			echo "################ Moving File from $lastDate to $prevDate ############" >> $backupFile
			cd $fullPath && mv -fv "${rangeFiles}" oldLogs >> $backupFile 2>&1
			echo "################ Compressing files $lastDate to $prevDate ###########" >> $backupFile
			cd $oldLogs && tar cfvz serverLog_"${lastDate}"-"${prevDate}""${count}".tar.gz "${rangeFiles}" --remove-files 1>>$backupFile 2>&1
			lastFile=$(cd $fullPath && ls -t server.log_* 2>/dev/null | tail -1)
			lastDate=$(cd $fullPath && date -r "${lastFile}" '+%Y-%m-%d' 2>/dev/null)
			prevDate=$(cd $fullPath && date -d "$lastDate+$range days" '+%Y-%m-%d')
			rangeFiles=$(cd $fullPath && find . -maxdepth 1 -type f -name 'server.log_*' -mtime +$rotate -newermt "$lastDate 00:00:01" ! -newermt "$prevDate 00:00:00")
			local count=$(("${count}" + 1))
		done
		echo "########################################################################################" >>$backupFile
	else
		while [ "$(df | awk 'FNR == 2 {print $(NF-2)}')" -le "${limit}" ]
		do
			echo "################ Deleting Following files on $today ###############" >> $backupFile
			cd $oldLogs && ls -t ./*.tar.gz > /dev/null 2>&1
			if [ $? -gt 0 ]; then
				echo "There is no more files to be deleted."  >> $backupFile
				break;
			fi
			cd $oldLogs && rm -fv "$(ls -t ./*.tar.gz | tail -1)" >> $backupFile
			echo -e "###################### Done #############################\n" >> $backupFile
		done
		echo "################ Nothing to do $today ###########" >> $backupFile
	fi
}

